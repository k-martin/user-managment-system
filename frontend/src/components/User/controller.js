

const BaseController = require('../../core/BaseController');
const template = require('./template.html');

class UserList extends BaseController {

    constructor(...args) {
        super(args);
    }

    index() {
        const self = this;

        this.request({
            url: '/users'
        }).then(function (data) {
            self.render(_.template(template)({users: data}));
        })
    }

    addUserForm() {
        debugger;
    }
}

module.exports = UserList;