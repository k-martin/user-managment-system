const config = require('../config');

class BaseController {

    constructor(...args) {
        this.rootElement = document.getElementById(config.rootElementId);
    }

    render (html) {
        this.rootElement.innerHTML = html;
    }

    request(params) {
        return $.ajax(params).promise();
    }

}

module.exports = BaseController;