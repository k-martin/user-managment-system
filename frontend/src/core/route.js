
class Route {

    constructor() {
        this.routes = {};
    }

    register(path, componentAction) {

        if (!componentAction) {
           throw Error('');
        }

        const [component, action] = componentAction.split('@');

        this.routes[path] = {
            component: component,
            action: action
        };
    }

    handler (e) {
debugger
        let url = location.hash.slice(1) || '/';

        let route = this.routes[url];

        if (route) {
            const controller = require(`../components/${route.component}/controller`);

            const obj = new controller();

            obj[route.action]();
        }
    }
}

const route = new Route;

window.addEventListener('hashchange', route.handler.bind(route));
window.addEventListener('load', route.handler.bind(route));

module.exports = route;