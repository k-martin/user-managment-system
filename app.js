const path = require('path');

global.appPath = path.resolve(__dirname);

require('./server/server');

require('./backend/routes');