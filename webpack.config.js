const webpack = require('webpack');
const path = require('path');
const config = require('./frontend/src/config');

module.exports = {
    entry: path.resolve(__dirname, './frontend/src/app.js'),
    output: {
        path: path.resolve(__dirname, './frontend/public/dist'),
        filename: 'ums.bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.html/,
                loader: "html-loader"
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }
        ],
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "window._": "lodash",
            "_": "lodash"
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "commons",
            filename: "commons.js",
            minChunks: 2
        })
    ]
};