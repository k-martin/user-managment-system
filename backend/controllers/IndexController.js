const path = require('path');

class IndexController {

    constructor(request, response) {
        this.request = request;
        this.response = response;
    }

    render() {
        this.response.sendFile(path.resolve(global.appPath, 'frontend/index.html'));
    }
}

module.exports = IndexController;