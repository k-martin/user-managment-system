const Route = require('../server/server')

const IndexController = require('./controllers/IndexController');
const UserController = require('./controllers/UserController');


Route.get('/', function (req, res) {
    const index = new IndexController(req, res);

    index.render();
})

Route.get('/users', function (req, res) {
    const index = new UserController(req, res);

    res.send([
        {
            username: "Banana",
            email: "akshdgjahsgd",
            age: 'asdasdasd'
        }
    ]);
})